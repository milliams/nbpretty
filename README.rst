nbpretty
========

nbpretty is a tool to convert sets of notebook files into a single, cohesive set of linked pages.

Documentation at https://nbpretty.readthedocs.io
