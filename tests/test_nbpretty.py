import textwrap
from pathlib import Path

import pytest
from bs4 import BeautifulSoup
from nbconvert import HTMLExporter
from traitlets.config import Config

import nbformat
from nbpretty.preprocessors import HighlightExercises, InsertTOC, SetTitle, HideWriteFileMagic
from nbpretty.toc import TOCTree, get_headers, construct_toc
from nbpretty import utils
from nbclient import NotebookClient


@pytest.fixture
def cwd():
    return Path(__file__).parent.absolute()


@pytest.fixture
def writefile():
    notebook = nbformat.v4.new_notebook()
    notebook.cells.append(nbformat.v4.new_code_cell("%%writefile foo.py\nimport sys\nprint(sys.path)"))
    notebook.cells.append(nbformat.v4.new_code_cell("!cat foo.py"))
    notebook.cells.append(nbformat.v4.new_code_cell("%run foo.py"))
    notebook.cells.append(nbformat.v4.new_code_cell("%cd /"))
    notebook.cells.append(nbformat.v4.new_code_cell(r"%run foo.py arg1 arg2 --flag=a"))
    notebook.cells.append(nbformat.v4.new_code_cell("!$sys.executable foo.py"))
    notebook.cells.append(nbformat.v4.new_code_cell("import sys\n!$sys.executable foo.py"))
    notebook.cells.append(nbformat.v4.new_code_cell("%%writefile foo.txt\nsome text"))
    nbformat.validate(notebook)
    return notebook


@pytest.fixture
def config():
    import nbpretty
    config = Config()
    config.TemplateExporter.extra_template_basedirs = [str(Path(nbpretty.__file__).parent.absolute() / "templates"), "/tmp/foo"]
    return config


def test_exercise_label(cwd, config):
    notebook = nbformat.v4.new_notebook()
    notebook.cells.append(nbformat.v4.new_markdown_cell("This is not an exercise"))
    notebook.cells.append(nbformat.v4.new_markdown_cell("### Exercise\n\nBut this is", metadata={"tags": ["exercise"]}))
    nbformat.validate(notebook)

    html_exporter = HTMLExporter(template_name="nbpretty", config=config)
    html_exporter.register_preprocessor(HighlightExercises, enabled=True)
    body, resources = html_exporter.from_notebook_node(notebook)
    soup = BeautifulSoup(body, features="html.parser")
    text_cells = soup.select("div.text_cell")
    assert "exercise" not in text_cells[0].attrs["class"]
    assert "exercise" in text_cells[1].attrs["class"]


def test_number_exercises(config):
    notebook = nbformat.v4.new_notebook()
    notebook.cells.append(nbformat.v4.new_markdown_cell("### Exercise #", metadata={"tags": ["exercise"]}))
    notebook.cells.append(nbformat.v4.new_markdown_cell("### Exercise #", metadata={"tags": ["exercise"]}))
    notebook.cells.append(nbformat.v4.new_markdown_cell("### Exercise #", metadata={"tags": ["exercise"]}))
    nbformat.validate(notebook)

    html_exporter = HTMLExporter(template_name="nbpretty", config=config)
    html_exporter.register_preprocessor(HighlightExercises, enabled=True)
    body, resources = html_exporter.from_notebook_node(notebook)
    soup = BeautifulSoup(body, features="html.parser")

    h3_cells = soup.select("div.text_cell_render h3")
    assert "Exercise 1" in h3_cells[0].text
    assert "Exercise 2" in h3_cells[1].text
    assert "Exercise 3" in h3_cells[2].text


def test_get_headers():
    html = """
<html>
<h1 id="main-title">Course Title</h1>
<h1 id="First">First<a class="anchor-link" href="#First">&#182;</a></h1>
<h2 id="Second">Second<a class="anchor-link" href="#Second">&#182;</a></h2>
<h3 id="Second-Sub">Second Sub<a class="anchor-link" href="#Second-Sub">&#182;</a></h3>
<h3 id="Next">Next<a class="anchor-link" href="#Next">&#182;</a></h3>
<h2 id="Last">Last<a class="anchor-link" href="#Last">&#182;</a></h2>
</html>
    """
    s = get_headers(html)
    expected = TOCTree(None, None, [
        TOCTree("First", "", [
            TOCTree("Second", "Second", [
                TOCTree("Second Sub", "Second-Sub"),
                TOCTree("Next", "Next"),
            ]),
            TOCTree("Last", "Last"),
        ]),
    ])
    assert s == expected


def test_toctree_html():
    toc = TOCTree(None, None, [
        TOCTree("First", "First", [
            TOCTree("Second", "Second"),
            TOCTree("Third", "Third"),
        ]),
        TOCTree("Fourth", "Fourth", [
            TOCTree("Fifth", "Fifth"),
        ]),
    ])

    expected2 = textwrap.dedent("""
    <ol>
     <li><a href="foo.html#First">First</a>
      <ol>
       <li><a href="foo.html#Second">Second</a></li>
       <li><a href="foo.html#Third">Third</a></li>
      </ol>
     </li>
     <li><a href="foo.html#Fourth">Fourth</a>
      <ol>
       <li><a href="foo.html#Fifth">Fifth</a></li>
      </ol>
     </li>
    </ol>
    """).strip()
    html = toc.to_html_list("foo.html", max_depth=2, wrap=True)
    assert html == expected2

    expected1 = textwrap.dedent("""
    <ol>
     <li><a href="foo.html#First">First</a></li>
     <li><a href="foo.html#Fourth">Fourth</a></li>
    </ol>
    """).strip()
    html = toc.to_html_list("foo.html", max_depth=1, wrap=True)
    assert html == expected1

    expected0 = ""
    html = toc.to_html_list("foo.html", max_depth=0, wrap=True)
    assert html == expected0


def test_insert_toc(cwd, config):
    with open(cwd / "toc_test.ipynb") as f:
        notebook = nbformat.read(f, as_version=4)

    toc_html = construct_toc([cwd / "toc_test.ipynb"], None)

    html_exporter = HTMLExporter(template_name="nbpretty", config=config)
    html_exporter.register_preprocessor(InsertTOC(toc_html), enabled=True)
    body, resources = html_exporter.from_notebook_node(notebook)
    soup = BeautifulSoup(body, features="html.parser")
    text_cells = soup.select("div.text_cell_render > ol")
    assert len(text_cells) == 1


def test_set_title(cwd, config):
    html_exporter = HTMLExporter(template_name="nbpretty", config=config)
    html_exporter.register_preprocessor(SetTitle("This Course"), enabled=True)

    body, resources = utils.ipynb_to_html(html_exporter, cwd / "00 Empty.ipynb")

    soup = BeautifulSoup(body, features="html.parser")
    text_cells = soup.find("head").find("title").string
    assert text_cells == "Empty - This Course"


def test_hide_writefile_magic_interactive_system():
    notebook = nbformat.v4.new_notebook()
    notebook.cells.append(nbformat.v4.new_code_cell("%load_ext interactive_system_magic"))
    notebook.cells.append(nbformat.v4.new_code_cell("%%prog cat\nfoo"))
    notebook.cells.append(nbformat.v4.new_code_cell("%%prog -i cat\nfoo"))
    notebook.cells.append(nbformat.v4.new_code_cell("print(1 + 2)"))
    notebook.cells.append(nbformat.v4.new_code_cell("%%writefile foo.py\nprint(57 + 94)"))
    notebook.cells.append(nbformat.v4.new_code_cell("%run_python_script foo.py"))
    notebook.cells.append(nbformat.v4.new_code_cell("%%run_python_script foo.py\nblah"))
    nbformat.validate(notebook)

    client = NotebookClient(notebook)
    executed = client.execute()
    nbformat.validate(executed)

    proc = HideWriteFileMagic()
    nb, _ = proc.preprocess(executed, {})
    nbformat.validate(nb)

    assert "runcommand" in nb["cells"][1]["metadata"]
    assert nb["cells"][1]["source"] == "cat"
    assert nb["cells"][1]["outputs"][0]["text"] == "foo"

    assert "runcommand" in nb["cells"][2]["metadata"]
    assert nb["cells"][2]["source"] == "cat"
    assert nb["cells"][2]["outputs"][0]["text"] == "foo\nfoo"

    assert "runcommand" not in nb["cells"][3]["metadata"]
    assert nb["cells"][3]["outputs"][0]["text"] == "3\n"

    assert "runcommand" in nb["cells"][5]["metadata"]
    assert nb["cells"][5]["source"] == "python foo.py"
    assert nb["cells"][5]["outputs"][0]["text"] == "151"

    assert "runcommand" in nb["cells"][6]["metadata"]
    assert nb["cells"][6]["source"] == "python foo.py"
    assert nb["cells"][6]["outputs"][0]["text"] == "151"


def test_hide_writefile_magic(writefile):
    proc = HideWriteFileMagic()
    nb, _ = proc.preprocess(writefile, {})

    assert nb["cells"][0]["metadata"]["writefile"] == "foo.py"
    assert nb["cells"][0]["metadata"]["magics_language"] == "python"
    assert "writefile" not in nb["cells"][0]["source"]

    assert "runcommand" in nb["cells"][1]["metadata"]
    assert nb["cells"][1]["metadata"]["magics_language"] == "bash"
    assert "!" not in nb["cells"][1]["source"]

    assert "runcommand" in nb["cells"][2]["metadata"]
    assert nb["cells"][2]["metadata"]["magics_language"] == "bash"
    assert "python" in nb["cells"][2]["source"]

    assert "runcommand" in nb["cells"][3]["metadata"]
    assert nb["cells"][3]["metadata"]["magics_language"] == "bash"
    assert "cd" in nb["cells"][3]["source"]
    assert "%cd" not in nb["cells"][3]["source"]

    assert "runcommand" in nb["cells"][4]["metadata"]
    assert nb["cells"][4]["metadata"]["magics_language"] == "bash"
    assert nb["cells"][4]["source"] == "python foo.py arg1 arg2 --flag=a"
    assert "%cd" not in nb["cells"][4]["source"]

    assert "runcommand" in nb["cells"][5]["metadata"]
    assert nb["cells"][5]["metadata"]["magics_language"] == "bash"
    assert nb["cells"][5]["source"] == "python foo.py"

    assert "runcommand" in nb["cells"][6]["metadata"]
    assert nb["cells"][6]["metadata"]["magics_language"] == "bash"
    assert nb["cells"][6]["source"] == "python foo.py"

    assert nb["cells"][7]["metadata"]["writefile"] == "foo.txt"
    assert nb["cells"][7]["metadata"]["magics_language"] == "text"
    assert "writefile" not in nb["cells"][7]["source"]


def test_highlight_run_script(writefile, config):
    """
    By default all cells will be highlighted as Python code.
    This checkes that the preprocessor is correctly telling nbconvert
    to use the bash highlighter for %run cells.
    """

    html_exporter = HTMLExporter(template_name="nbpretty", config=config)
    html_exporter.register_preprocessor(HideWriteFileMagic, enabled=True)
    body, resources = html_exporter.from_notebook_node(writefile)
    soup = BeautifulSoup(body, features="html.parser")
    text_cells = soup.select(".code_cell .input_area pre")

    assert "foo.py" in str(text_cells[2])
