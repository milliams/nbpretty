Contributing
============

Build the docs
--------------

To build the docs for local development, you can use ``sphinx-autobuild``:

.. code-block:: shell-session

   $ poetry run sphinx-autobuild docs docs/_build/html
