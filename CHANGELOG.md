# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.3] - 2024-06-03

### Fixed

- Use markupsafe for removed jinja2 functionality

## [0.3.2] - 2024-04-11

### Fixed

- Pass list of strings to `extra_template_basedirs` to avoid trailets error. [Issue #1](https://gitlab.com/milliams/nbpretty/-/issues/1)

## [0.3.1] - 2024-04-11

### Changed

- Updated package versions

## [0.3.0] - 2024-01-08

### Added

- Add highlighting for C++ and Rust files

## [0.2.14] - 2022-08-03

### Fixed

- Correct the syntax highlighting of %%writefile cells

## [0.2.13] - 2022-07-26

### Fixed

- Correctly rewrite Python run cell magic

## [0.2.12] - 2022-07-26

### Fixed

- Typo in stream type causing crash

## [0.2.11] - 2022-07-26

### Added

- Support for `interactive-system-magic`

## [0.2.10] - 2022-07-07

### Added

- Reparse `!$sys.executable` as `python`

## [0.2.9] - 2022-07-05

### Changed

- Prefix generated CSS filenames with "nbpretty_"

### Fixed

- Don't strip arguments from `%run` magics

## [0.2.8] - 2022-04-04

### Added

- Add custom CSS with `extra_css_file`

## [0.2.7] - 2022-04-01

### Fixed

- Set Jinja2 version

## [0.2.6] - 2022-03-23

### Added

- Remove cell inputs with `remove_input` cell tag

## [0.2.5] - 2022-02-14

### Changed

- Fix rendering of code in rendered cells

## [0.2.4] - 2022-01-18

### Changed

- Use fast validation of notebooks - gives a 3x speedup

## [0.2.3] - 2021-12-13

### Fixed

- Correctly highlight %run cells as Bash

## [0.2.2] - 2021-12-13

### Added

- Auto-number exercises marked with #

### Fixed

- Allow chapter names to start with numbers

## [0.2.1] - 2021-09-27

### Fixed

- re-release to fix missing files

## [0.2.0] - 2021-09-27

### Changed

- Update to nbconvert 6

### Fixed

- answer and appendix pages now have correct title

## [0.1.6] - 2021-03-19

### Changed

- Made the display of exercises a little nicer

## [0.1.5] - 2020-11-18

### Changed

- Only show drop shadow on Markdown images
- Format definition lists better

## [0.1.4] - 2020-09-22

### Changed

- Tweaked CSS to add code padding

## [0.1.3] - 2020-09-15

### Added

- Convert most-recently modified notebooks first

## [0.1.2] - 2020-09-14

### Added

- Add a --serve flag to display in browser with auto-reloading
- Auto-build on change when serving

### Fixed

- Correctly look for config.yaml in the notebook directory
- Write output to source directory

## [0.1.1] - 2020-09-08

### Added

- Added details to packaging

## [0.1.0] - 2020-09-08

### Added

- Initial release

[//]: # (C3-2-DKAC)
