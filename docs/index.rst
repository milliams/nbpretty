nbpretty
========

``nbpretty`` is a tool which can convert a set of Jupyter Notebook files into a linked set of HTML pages.
Its primary use is creating teaching material for programming.

To get started, have a go through the tutorial:

.. toctree::
   :caption: Tutorials
   :titlesonly:

   tutorial

Once you've got some material written, have a look at some of the how-tos to see if they help with any issues you have come up:

.. toctree::
   :caption: How-to guides
   :titlesonly:

   write-and-run-scripts
   custom-css
   ci

To see what's going on behind the scenes, you can see some reference material for the :program:`nbconvert` preprocessors:

.. toctree::
   :caption: Reference
   :titlesonly:

   preprocessors


