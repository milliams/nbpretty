Write and run scripts
=====================

Jupyter Notebooks are a good way of experimenting in Python code and are often used as an interface for teaching.
However, a significant amount of programming training uses script file and command-line tools and so :program:`nbpretty` needs to be able to support these too.
In this way, the fact that you have used a notebook to write your notes is irrelevant to the student as they will be working in another environment entirely.
You can see an example of this in the course notes for `Beginning Python <https://milliams.com/courses/beginning_python/>`_.

Write file
----------

If you put Python code in a code cell in a notebook and run it, the code and the output will appear in the HTML.
If you want to direct a student to write some text in a file and run it you can use the standard ``%%writefile`` `magic from IPython <https://ipython.readthedocs.io/en/stable/interactive/magics.html#cellmagic-writefile>`__.
A code cell with the following:

.. code-block:: ipython

   %%writefile foo.py

   print("hello")

will display as something like:

.. code-block:: python
   :caption: foo.py

   print("hello")

Run command
-----------

You can use standard IPython techniques for running commands.
If you want to run a Python script, you can do it with the ``%run`` `magic <https://ipython.readthedocs.io/en/stable/interactive/magics.html#magic-run>`__:

.. code-block:: ipython

   %run foo.py

or with a call to the underlying console with:

.. code-block:: ipython

   !python foo.py

Both of these will be formatted in the output HTML to look like:

.. code-block:: shell-session

   $ python foo.py

This all means that if you run your notebook in  some automated CI system, the first cell will create the file as needed, and the second will run it exactly as the student would.

Other tools
-----------

More generally than just Python scripts, any notebook cell with:

.. code-block:: ipython

   !some-command args

will be formatted as:

.. code-block:: shell-session

   $ some-command args
