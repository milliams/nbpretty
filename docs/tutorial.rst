Getting started
===============

Setup
-----

The first thing you'll need to do is make a directory on your computer to hold your documentation pages:

.. code-block:: shell-session

   $ mkdir my_docs
   $ cd my_docs

If you're used to Python development you'll know there are a bunch of different ways to set up a project which uses Python tools.
One way is to create a Python package which contains your tools as dependencies (i.e. a :file:`requirements.txt` file or using something like `Poetry <https://python-poetry.org/>`_).
Another way is to install the tools you are using separately.
We'll go with the latter method here as you can use :program:`nbprett to develop any kind of documentation in any language.

We'll use a tool called `pipx <https://github.com/pypa/pipx>`_ to install our tools but you can use any method you like:

.. code-block:: shell-session

   $ pipx install nbpretty

If we now go ahead and run the tool we'll see something like:

.. code-block:: shell-session

   $ nbpretty .
   Usage: nbpretty [OPTIONS] DIRECTORY
   Try 'nbpretty --help' for help.

   Error: config.yaml could not be found

:program:`nbpretty` is designed to be config-light, but there are some details that it needs before it can build anything, in this case it needs the name of the "book" that you are creating.
This is specified in a file called :file:`config.yaml` and just needs the following:

.. code-block:: yaml
   :caption: config.yaml

   ---
   course_title: My first course

We're now ready to start writing the material.

Create your first page
----------------------

The pages in :program:`nbpretty` are each written as Jupyter Notebooks so launch a JupyterLab interface in any way you'd like and create an empty notebook.

Save the notebook as :file:`00 Introduction.ipynb`.
The leading numbers are used to order the pages, the space is important, and the rest of the file name is used to name the HTML output file and the title of the page.

If you now run ``nbpretty`` again, you'll see something like:

.. code-block:: shell-session

   $ nbpretty .
   INFO     Constructing table of contents
   INFO     Writing '00 Introduction.ipynb' as 'index.html'

So it's used the lowest numbered file as the starting page for the book.
You can go and open that file directly in a browser.

You'll likely find it easier to use the :option:`--serve` argument which will automatically open the generated HTML in a browser and rebuild it it when you make any changes:

.. code-block:: shell-session

   $ nbpretty . --serve
   INFO     Constructing table of contents
   INFO     Writing '00 Introduction.ipynb' as 'index.html'
   INFO     Serving on http://127.0.0.1:5500
   INFO     Start watching changes
   INFO     Start detecting changes
   ...

You should see a page open in your browser with the text "My first course" at the top, followed by the empty code cell in your notebook.

Try adding some markdown and code cells to the notebook and save it.
You should see them appear in the generated page.

Add more pages
--------------

Create a second notebook with the name :file:`01 The Next Page.ipynb` and make sure it's saved.

If you look at the generated page now, you should see a "Next" link appeared at the bottom which takes you to the next page.

You can add as many pages you like in this same way.
:program:`nbpretty` will use the number in front of the title in the file name to order them.
You can leave gaps in the numbering if you like, so you can have a :file:`99 Summary.ipynb` file which avoids renumbering as you develop your materials.

Table of contents
'''''''''''''''''

You can add a book-wide table of contents to a page by creating an empty markdown cell and adding the ``toc`` tag to the cell.

Add one of these to the beginning of the :file:`00 Introduction.ipynb` notebook, and add a Markdown cell to the top of the :file:`01 The Next Page.ipynb` page containing:

.. code-block:: markdown

   # The next page

Make sure that both notebook are saved and you should see the table of contents appear on the first page.

Try adding some more pages and give each of those a title too.
You will see the table of contents update as you go.

Exercises
---------

You will likely want to be able to set the students some exercises and it's useful if these are highlighted.
And Markdown cell can be tabbed with the ``exercise`` tag and it will show up specially in the output.

You can also literally put

.. code-block:: markdown

   ### Exercise #

at the beginning of the Markdown cell and it will automatically number the exercises on each page.

Special page types
------------------

``nbpretty`` does not process every notebook file in the folder, only those that are named in a certain way.
We've seen that it converts files named :file:`{NN Title}.ipynb` but there are some other patterns that it converts.

One in particular that you might want is :file:`answer_{foo}.ipynb` where "foo" can be anything you like.
These will be processed and will create a corresponding :file:`answer_{foo}.html` file.

These special pages will not be linked to automatically, but you can create a standard Markdown hyperlink to :file:`answer_{foo}.ipynb` which will be converted to a link to :file:`answer_{foo}.html` in the generated HTML.

Wrap-up
-------

This should be enough to get you started with :program:`nbpretty`.
Anything you can put in a notebook should work as you expect.
