Preprocessors
=============

All of the following preprocessors are run over the notebooks.

:py:class:`nbconvert.preprocessors.TagRemovePreprocessor`
   This runs the standard tag-remover and removes any cells with the tag ``remove_cell``.

.. autoclass:: nbpretty.preprocessors.PageLinks

.. autoclass:: nbpretty.preprocessors.HighlightExercises

.. autoclass:: nbpretty.preprocessors.SetTitle

.. autoclass:: nbpretty.preprocessors.HideWriteFileMagic

.. autoclass:: nbpretty.preprocessors.FixLinkExtensions

.. autoclass:: nbpretty.preprocessors.CustomBlocks

.. autoclass:: nbpretty.preprocessors.InsertTOC

.. autoclass:: nbpretty.preprocessors.UninlineCss

.. autoclass:: nbpretty.preprocessors.AddExtraCss
