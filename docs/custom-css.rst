Adding custom CSS
=================

In :file:`config.yaml` you can add a ``extra_css_file`` entry pointing to a CSS file which will be included on every page:

.. code-block:: yaml
   :caption: config.yaml

   ---
   course_title: My course
   extra_css_file: extra.css

Then in the CSS file you can put CSS you like:

.. code-block:: css
   :caption: extra.css

   body {
      font-size: 2rem;
   }

:program:`nbpretty` will automatically add the CSS to every generated HTML page after all other CSS files:

.. code-block:: html

   <style>@import 'extra.css'</style>
